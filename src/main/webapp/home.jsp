<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href="/head.css" rel="stylesheet" type="text/css">
    <title>Head page</title>
</head>
<body>

<div id="panel"> >
    <div id="hidden_panel">
        <a href="/shop/"> <button type="submit" class="butt">КАБИНЕТ</button></a>
        <br>
        <a href="/shop/"> <button type="submit" class="butt">КАТАЛОГ</button></a>
        <br>
        <a href="#" onclick="slowScroll('#main')"> <button type="submit" class="butt">О КОМПАНИИ</button></a>
        <br>
        <a href="#" onclick="slowScroll('#overview')"> <button type="submit" class="butt">ГАЛЕРЕЯ</button></a>
        <br>
        <a href="/shop/bas/"> <button type="submit" class="butt">КОРЗИНА</button></a>
        <br>
        <a href="/shop/exit"> <button type="submit" class="butt">ВЫХОД</button></a>

    </div>
</div>
<div id="intro">
    <div class="video">
        <video class="video__media" src="/videos/2.mp4" autoplay muted loop></video>
    </div>

    <div class="intro__content">
        <div class="container1">
            <img src="/images/logo.png">
        </div>
    </div>
</div>


<div id="main">
    <div class="intro">
        <h2>О Компании</h2>
    </div>
    <div class="text">
        <span>Основное назначение автомобиля заключается в совершении транспортной работы[2].
            Автомобильный транспорт в промышленно развитых странах занимает ведущее место по сравнению
            с другими видами транспорта по объёму перевозок пассажиров[3]. Современный автомобиль состоит
            из 15—20 тысяч деталей, из которых 150—300 являются наиболее важными и требующими наибольших
            затрат в эксплуатации[4]. Понятие включает легковой автомобиль, грузовой автомобиль, автобус,
            троллейбус, бронетранспортёр, но не включает сельскохозяйственный трактор и мотоцикл.Основное
            назначение автомобиля заключается в совершении транспортной работы[2]. Автомобильный транспорт
            в промышленно развитых странах занимает ведущее место по сравнению с другими видами транспорта
            по объёму перевозок пассажиров[3]. Современный автомобиль состоит из 15—20 тысяч деталей, из
            которых 150—300 являются наиболее важными и требующими наибольших затрат в эксплуатации[4].
            Понятие включает легковой автомобиль, грузовой автомобиль, автобус, троллейбус, бронетранспортёр,
            но не включает сельскохозяйственный трактор и мотоцикл.</span>
    </div>
</div>

<div id="overview">
    <div class="row">
        <div class="col-75">
            <div class="container">
                <div class="all">
                    <input checked type="radio" name="respond" id="desktop">
                    <article id="slider">
                        <input checked type="radio" name="slider" id="switch1">
                        <input type="radio" name="slider" id="switch2">
                        <input type="radio" name="slider" id="switch3">
                        <input type="radio" name="slider" id="switch4">
                        <div id="slides">
                            <div id="overflow">
                                <div class="image">
                                    <article><img src='<c:url value="/images/car1.jpg"></c:url>'/></article>
                                    <article><img src='<c:url value="/images/car33.jpg"></c:url>'/></article>
                                    <article><img src='<c:url value="/images/car55.jpg"></c:url>'/></article>
                                    <article><img src='<c:url value="/images/car88.jpg"></c:url>'/></article>
                                </div>
                            </div>
                        </div>
                        <div id="controls">
                            <label for="switch1"></label>
                            <label for="switch2"></label>
                            <label for="switch3"></label>
                            <label for="switch4"></label>
                        </div>
                        <div id="active">
                            <label for="switch1"></label>
                            <label for="switch2"></label>
                            <label for="switch3"></label>
                            <label for="switch4"></label>
                        </div>
                    </article>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    function slowScroll(id) {
        $('html, body').animate({
            scrollTop: $(id).offset().top
        }, 500);
    }

    $(document).on("scroll", function () {
        if ($(window).scrollTop() === 0)
            $("header").removeClass("fixed");
        else
            $("header").attr("class", "fixed");
    });
</script>
</body>
</html>
