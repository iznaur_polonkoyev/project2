<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href="/pay.css" rel="stylesheet" type="text/css">
    <title> Products</title>
</head>
<body>

<div id="overview">
    <h1>СПАСИБО ЗА ПОКУПКУ!</h1>
    <a href="/shop/">
        <button type="submit" class="butt">ВЕРНУТЬСЯ В МЕНЮ ТОВАРОВ</button>
    </a>
    <div class="spravca">
        <input type="checkbox" id="hd-1" class="reference"/>
        <label for="hd-1">Детали заказа</label>
        <span class="story">

        <div class="col-75">
            <div class="intro__content">
                <div class="container">
                    <div class="row">
                        <div class="col-50">
                            <h3>Платежный адрес</h3>

                            <label for="name"><i class="fa fa-user"></i> Имя</label>
                            <input type="text" id="name" name="name" value="${userName}" disabled>
                            <label for="surname"><i class="fa fa-user"></i> Фамилия</label>
                            <input type="text" id="surname" name="surname" value="${userSurname}" disabled>
                            <label for="email"><i class="fa fa-envelope"></i> Email</label>
                            <input type="text" id="email" name="email" value="${userEmail}" disabled>
                            <label for="address"><i class="fa fa-address-card-o"></i> Адрес</label>
                            <input type="text" id="address" name="address" value="${userAddress}" disabled>
                            <label for="city"><i class="fa fa-institution"></i> Город</label>
                            <input type="text" id="city" name="city" value="${userCity}" disabled>
                        </div>
                        <div class="col-50">
                            <h4>Cart <span class="price" style="color:black"><i
                                    class="fa fa-shopping-cart"></i> <b>${list.size()}</b></span></h4>
                            <c:forEach var="item" items="${list}">
                                <p><a href="#">
                                    <td>${item.brand}</td>
                                    <td>${item.name}</td>
                                </a><span class="price"><td>$${item.price}</td></span></p>
                                <hr>
                            </c:forEach>
                            <p>Всего <span class="price" style="color:black"><b>${allPrice}$</b></span></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</span>
    </div>
</div>
</body>


</html>
