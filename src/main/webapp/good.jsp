<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href="/good.css" rel="stylesheet" type="text/css">
    <title> Products</title>
</head>
<body>
<div id="panel"> >
    <div id="hidden_panel">
        <a href="/shop/"> <button type="submit" class="butt1">КАБИНЕТ</button></a>
        <br>
        <a href="/shop/login"> <button type="submit" class="butt1">ГЛАВНАЯ</button></a>
        <br>
        <a href="/shop/"> <button type="submit" class="butt1">КАТАЛОГ</button></a>
        <br>
        <a href="/shop/bas/"> <button type="submit" class="butt1">КОРЗИНА</button></a>
        <br>
        <a href="/shop/exit"> <button type="submit" class="butt1">ВЫХОД</button></a>

    </div>
</div>

<div class="row">
    <div class="col-75">
        <div class="container">
            <div class="all">
                <input checked type="radio" name="respond" id="desktop">
                <article id="slider">
                    <input checked type="radio" name="slider" id="switch1">
                    <input type="radio" name="slider" id="switch2">
                    <input type="radio" name="slider" id="switch3">
                    <input type="radio" name="slider" id="switch4">
                    <div id="slides">
                        <div id="overflow">
                            <div class="image">
                                <c:forEach var="item" items="${list}">
                                    <article>
                                        <tr><img src="${item.icon_base64_icon}"/></tr>
                                    </article>
                                </c:forEach>
                            </div>
                        </div>
                    </div>
                    <div id="controls">
                        <label for="switch1"></label>
                        <label for="switch2"></label>
                        <label for="switch3"></label>
                        <label for="switch4"></label>
                    </div>
                    <div id="active">
                        <label for="switch1"></label>
                        <label for="switch2"></label>
                        <label for="switch3"></label>
                        <label for="switch4"></label>
                    </div>
                </article>
            </div>


        </div>
    </div>
    <div class="col-25">
        <div class="container">

            <c:forEach var="item" items="${list}" begin="0" end="0">
                <h1>
                    <td>${item.brand}</td>
                    <td>${item.name}</td>
                    <br>
                </h1>
                <div style="display:flex;justify-content:center;align-items:center;">
                    <form id="butt" class="butt" action="/shop/opa/${item.id}" method="post">
                        <a> <button type="submit" class="butt">Добавить в корзину</button></a>
                    </form>
                </div>
            </c:forEach>
        </div>
    </div>
</div>
<br>
<div class="row">
    <div class="col-75">
        <div class="container1">
            <h1>Описание</h1>
            <br>
            <c:forEach var="item" items="${list}" begin="0" end="0">

                <td><h2>${item.description}</h2></td>

            </c:forEach>
        </div>
    </div>
    <div class="col-25">
        <div class="container1">
            <h1>Характеристики</h1>
            <br>
            <c:forEach var="item" items="${list}" begin="0" end="0">

                <td><h2>Кузов: ${item.body}</h2></td>
                <br>
                <td><h2>Объем: ${item.spacity}</h2></td>
                <br>
                <td><h2>КПП: ${item.transm}</h2></td>
                <br>
                <td><h2>Цвет: ${item.color}</h2></td>

            </c:forEach>
        </div>
    </div>
</div>
</body>
</html>


