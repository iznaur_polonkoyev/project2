package spring.demo.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import spring.demo.dto.LoginDto;
import spring.demo.dto.OrderDto;
import spring.demo.dto.ProductsDto;
import spring.demo.dto.UserDto;
import spring.demo.util.Utils;

@Repository
public class PersonRepository {

    @Autowired
    private Utils utils;

    public UserDto checkLogin(LoginDto loginDto) {
        Connection connection = utils.getConnection();

        try (PreparedStatement statement = connection
                .prepareStatement("select * from public.users where email = ? and password = ?")) {

            String password = DigestUtils.md5Hex(loginDto.getPassword());
            statement.setString(1, loginDto.getEmail());
            statement.setString(2, password);
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                UserDto personDto = new UserDto();
                personDto.setId(resultSet.getInt("id"));
                personDto.setName(resultSet.getString("name"));
                personDto.setSurname(resultSet.getString("surname"));
                String session = updateSession(personDto.getId(), DigestUtils.md5Hex(String.valueOf(Math.random() * 1000)));
                personDto.setSession(session);
                return personDto;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private String updateSession(Integer id, String md5Hex) {
        System.out.println("ID: "+id);
        Connection connection = utils.getConnection();
        try (PreparedStatement statement = connection
                .prepareStatement("UPDATE public.users set session = ? where id = ?")) {

            statement.setString(1, md5Hex);
            statement.setInt(2, id);
            statement.executeUpdate();

            return md5Hex;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public UserDto getPerson(String session) {
        Connection connection = utils.getConnection();
        UserDto personDto = new UserDto();

        try (PreparedStatement statement = connection.prepareStatement("select * from users where session =?")) {
            statement.setString(1,session);
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {

                personDto.setId(resultSet.getInt("id"));
                personDto.setName(resultSet.getString("name"));
                personDto.setSurname(resultSet.getString("surname"));
                personDto.setEmail(resultSet.getString("email"));
                personDto.setAddress(resultSet.getString("address"));
                personDto.setCity(resultSet.getString("city"));

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return personDto;
    }
    public OrderDto getPersonOrder(String session) {
        Connection connection = utils.getConnection();
        OrderDto orderDto = new OrderDto();
        try (PreparedStatement statement = connection.prepareStatement("select * from card where session =?")) {
            statement.setString(1,session);
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {

                orderDto.setName(resultSet.getString("name"));
                orderDto.setSurname(resultSet.getString("surname"));
                orderDto.setEmail(resultSet.getString("email"));

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return orderDto;
    }

    public void savePerson(UserDto userDto, LoginDto loginDto) {
        try (Connection connection = utils.getConnection();

             PreparedStatement statement = connection
                     .prepareStatement("INSERT INTO users (email,password,name,surname) values (?, ?, ?, ?)")) {

            String password = DigestUtils.md5Hex(loginDto.getPassword());
            statement.setString(1, loginDto.getEmail());
            statement.setString(2, password);
            statement.setString(3, userDto.getName());
            statement.setString(4, userDto.getSurname());
            statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateAddress(Integer userId,UserDto userDto) {
        try (Connection connection = utils.getConnection();

             PreparedStatement statement = connection
                     .prepareStatement("UPDATE users set address = ?,city = ? where id =?")) {

            statement.setString(1, userDto.getAddress());
            statement.setString(2, userDto.getCity());
            statement.setInt(3, userId);


            statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ProductsDto getById(int id) {
        Connection connection = utils.getConnection();

        try (PreparedStatement statement = connection.prepareStatement("select * from products where id = ?")) {
            statement.setInt(1, id);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                ProductsDto productsDto = new ProductsDto();
                productsDto.setBrand(resultSet.getString("brand_product"));
                productsDto.setName(resultSet.getString("name_product"));
                productsDto.setId(resultSet.getInt("id"));
                productsDto.setIcon_base64(resultSet.getString("icon_base64"));

                return productsDto;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Integer getIdBySession(String session) throws Exception {
        Connection connection = utils.getConnection();

        try (PreparedStatement statement = connection.prepareStatement("select id from public.users where session = ?")) {
            statement.setString(1, session);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                return resultSet.getInt("id");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        throw new Exception("Not found user id by this session");
    }
}
