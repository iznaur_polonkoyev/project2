package spring.demo.repository;


import spring.demo.dto.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import spring.demo.util.Utils;
import org.w3c.dom.Text;

@Repository
public class ProductsRepository {

    @Autowired
    private Utils utils;

    public List<ProductsDto> getById(int id) {
        Connection connection = utils.getConnection();
        List<ProductsDto> list = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement("select * from products,images,spec where products.id =? and images.product_id= ? and spec.product_id = ?")) {
            statement.setInt(1, id);
            statement.setInt(2, id);
            statement.setInt(3, id);

            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                ProductsDto productsDto = new ProductsDto();
                productsDto.setBrand(resultSet.getString("brand_product"));
                productsDto.setName(resultSet.getString("name_product"));
                productsDto.setId(resultSet.getInt("id"));
                productsDto.setIcon_base64_icon(resultSet.getString("icon_base64_icon"));
                productsDto.setPrice(resultSet.getInt("price"));
                productsDto.setDescription(resultSet.getString("description"));
                productsDto.setBody(resultSet.getString("body"));
                productsDto.setSpacity(resultSet.getString("spacity"));
                productsDto.setTransm(resultSet.getString("transm"));
                productsDto.setColor(resultSet.getString("color"));
                productsDto.setCount(resultSet.getInt("count"));
                list.add(productsDto);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<ImagesDto> getByIdList(int id) {
        Connection connection = utils.getConnection();

        List<ImagesDto> list = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement("select * from products,images where products.id =? and images.product_id= ?")) {
            statement.setInt(1, id);
            statement.setInt(2, id);


            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                ImagesDto imagesDto = new ImagesDto();
                imagesDto.setName(resultSet.getString("name_product"));
                imagesDto.setBrand(resultSet.getString("brand_product"));
                imagesDto.setIcon_base64_icon(resultSet.getString("icon_base64_icon"));
                list.add(imagesDto);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<ProductsDto> getProducts() {
        Connection connection = utils.getConnection();

        List<ProductsDto> list = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement("select * from products where category_id=1")) {
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                ProductsDto productsDto = new ProductsDto();
                productsDto.setId(resultSet.getInt("id"));
                productsDto.setName(resultSet.getString("name_product"));
                productsDto.setBrand(resultSet.getString("brand_product"));
                productsDto.setIcon_base64(resultSet.getString("icon_base64"));
                list.add(productsDto);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<ProductsDto> getProducts1() {
        Connection connection = utils.getConnection();

        List<ProductsDto> list = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement("select * from products where category_id = 2")) {
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                ProductsDto productsDto = new ProductsDto();
                productsDto.setId(resultSet.getInt("id"));
                productsDto.setName(resultSet.getString("name_product"));
                productsDto.setBrand(resultSet.getString("brand_product"));
                productsDto.setIcon_base64(resultSet.getString("icon_base64"));
                list.add(productsDto);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<ProductsDto> getProducts2() {
        Connection connection = utils.getConnection();

        List<ProductsDto> list = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement("select * from products where category_id=3")) {
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                ProductsDto productsDto = new ProductsDto();
                productsDto.setId(resultSet.getInt("id"));
                productsDto.setName(resultSet.getString("name_product"));
                productsDto.setBrand(resultSet.getString("brand_product"));
                productsDto.setIcon_base64(resultSet.getString("icon_base64"));
                list.add(productsDto);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public OrderDto getCard(int userId) {
        Connection connection = utils.getConnection();

        OrderDto orderDto = new OrderDto();
        try (PreparedStatement statement = connection.prepareStatement("select * from card where user_id=?")) {
            statement.setInt(1, userId);

            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {

                orderDto.setName_card(resultSet.getString("name_card"));
                orderDto.setNumber_card(resultSet.getString("number_card"));
                orderDto.setCvc(resultSet.getString("cvc"));
                orderDto.setExp_date(resultSet.getString("exp_date"));
                System.out.println(orderDto.toString());

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return orderDto;
    }

    public List<ProductsDto> getProductsFromBasket(int userId) {
        Connection connection = utils.getConnection();

        List<ProductsDto> list = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement("select * from products where id in  (select product_id from basket where user_id = ?)")) {
            statement.setInt(1, userId);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                ProductsDto productsDto = new ProductsDto();
                productsDto.setId(resultSet.getInt("id"));
                productsDto.setName(resultSet.getString("name_product"));
                productsDto.setBrand(resultSet.getString("brand_product"));
                productsDto.setIcon_base64(resultSet.getString("icon_base64"));
                productsDto.setPrice(resultSet.getInt("price"));
                productsDto.setCount(resultSet.getInt("count"));
                list.add(productsDto);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public void saveIdToBasket(int id, Integer userId) {

        try (Connection connection = utils.getConnection();

             PreparedStatement statement = connection
                     .prepareStatement("INSERT INTO basket (product_id,user_id) values (?,?)")) {

            statement.setInt(1, id);
            statement.setInt(2, userId);
            statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateCounters(Integer userId,Integer allPrice) {

        try (Connection connection = utils.getConnection();

             PreparedStatement statement = connection
                     .prepareStatement("UPDATE card set cash = cash-? where user_id =?")) {

            statement.setInt(1, allPrice);
            statement.setInt(2, userId);
            statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteIdToBasket(int id, Integer userId) {

        try (Connection connection = utils.getConnection();

             PreparedStatement statement = connection
                     .prepareStatement("delete from basket where product_id = ? and user_id =?")) {

            statement.setInt(1, id);
            statement.setInt(2, userId);
            statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteAllToBasket(Integer userId) {

        try (Connection connection = utils.getConnection();

             PreparedStatement statement = connection
                     .prepareStatement("delete from basket where user_id =?")) {


            statement.setInt(1, userId);
            statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
