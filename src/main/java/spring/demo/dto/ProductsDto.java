package spring.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.w3c.dom.Text;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ProductsDto {
    private Integer id;
    private String name;
    private String brand;
    private String icon_base64;
    private String icon_base64_icon;
    private Integer price;
    private String description;
    private String body;
    private String spacity;
    private String transm;
    private String color;
    private Integer count;

}