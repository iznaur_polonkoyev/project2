package spring.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class OrderDto {
    private String name;
    private String surname;
    private String email;
    private String name_card;
    private String number_card;
    private String cvc;
    private String exp_date;
    private String address;
    private String city;

}