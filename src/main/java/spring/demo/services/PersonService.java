package spring.demo.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spring.demo.dto.LoginDto;
import spring.demo.dto.ProductsDto;
import spring.demo.dto.UserDto;
import spring.demo.repository.PersonRepository;

@Service
public class PersonService {


  @Autowired
  private PersonRepository personRepository;


  public UserDto checkLogin(LoginDto loginDto) {

    return personRepository.checkLogin(loginDto);
  }

  public ProductsDto getProductById(int id) {
    ProductsDto productsDto = personRepository.getById(id);
    return productsDto;
  }

  public Integer getIdBySession(String session) throws Exception {
    return personRepository.getIdBySession(session);
  }

  public void updateAddress(Integer userId,UserDto userDto) throws Exception {

    personRepository.updateAddress(userId,userDto);
  }

  public void addPerson(UserDto userDto,LoginDto loginDto) {
    personRepository.savePerson(userDto,loginDto);
  }
//  public List<UserDto> getPersons() {
//    List<UserDto> list = personRepository.getPersons();
//
//    for (UserDto item : list) {
//      item.setContacts(contactRepository.getContactsByPersonId(item.getId()));
//    }
//
//    return list;
//  }
}
