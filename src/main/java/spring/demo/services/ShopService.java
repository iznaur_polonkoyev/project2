package spring.demo.services;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spring.demo.dto.*;
import spring.demo.repository.PersonRepository;
import spring.demo.repository.ProductsRepository;

@Service
public class ShopService {

    @Autowired
    private ProductsRepository productsRepository;

    @Autowired
    private PersonRepository personRepository;

    public List<ProductsDto> getProducts() {
        List<ProductsDto> list = productsRepository.getProducts();
        return list;
    }

    public List<ProductsDto> getProducts1() {
        List<ProductsDto> list1 = productsRepository.getProducts1();
        return list1;
    }
    public List<ProductsDto> getProducts2() {
        List<ProductsDto> list2 = productsRepository.getProducts2();
        return list2;
    }

    public OrderDto getCard(int userId) {
        OrderDto orderDto = productsRepository.getCard(userId);
        return orderDto;
    }
    public List<ProductsDto> getProductsBasket(int userId) {
        List<ProductsDto> list = productsRepository.getProductsFromBasket(userId);
        return list;
    }


    public List<ProductsDto> getProductById(int id) {
        List<ProductsDto> list = productsRepository.getById(id);
        return list;
    }
    public List<ImagesDto> getProductByIdList(int id) {
        List<ImagesDto> list = productsRepository.getByIdList(id);
        return list;
    }


    public void addId(int id, Integer userId) throws Exception {
        productsRepository.saveIdToBasket(id,userId);
    }

    public void deleteId(int id, Integer userId) throws Exception {

        productsRepository.deleteIdToBasket(id,userId);
    }
    public void deleteAll(Integer userId) throws Exception {

        productsRepository.deleteAllToBasket(userId);
    }

    public void updateCounters(Integer userId,Integer allPrice) throws Exception {
        productsRepository.updateCounters(userId,allPrice);
    }

}
