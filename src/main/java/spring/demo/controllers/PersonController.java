package spring.demo.controllers;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import spring.demo.dto.LoginDto;
import spring.demo.dto.ProductsDto;
import spring.demo.dto.UserDto;
import spring.demo.services.PersonService;
import spring.demo.util.SessionUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


@Controller
@RequestMapping("/person")
public class PersonController {

    @Autowired
    private PersonService personService;

    @Autowired
    private SessionUtil sessionUtil;

    static Integer id = 0;

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ModelAndView passParametersWithModelAndView(@RequestParam(value = "username", required = false) String username,
                                                       @RequestParam(value = "password", required = false) String password,
                                                       HttpServletRequest request) throws Exception {

        LoginDto loginDto = new LoginDto();
        loginDto.setEmail(username);
        loginDto.setPassword(password);
        UserDto userDto = personService.checkLogin(loginDto);

        if (Objects.isNull(userDto)) {
            id = userDto.getId();
            System.out.println(id);
            ModelAndView modelAndView = new ModelAndView("index");
            modelAndView.addObject("result", "Error login");
            return modelAndView;
        } else {
            ModelAndView modelAndView = new ModelAndView("home");
            modelAndView.addObject("result", "Correct login");
            modelAndView.addObject("id", userDto.getId());
            modelAndView.addObject("name", userDto.getName());
            modelAndView.addObject("surname", userDto.getSurname());
            HttpSession session = request.getSession();
            System.out.println("SESSION!!!: "+session);
            session.setAttribute("session", userDto.getSession());
            return modelAndView;
        }
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)

    public ModelAndView Registration(@RequestParam(value = "email", required = false) String email,
                                     @RequestParam(value = "password", required = false) String password,
                                     @RequestParam(value = "name", required = false) String name,
                                     @RequestParam(value = "surname", required = false) String surname) {

        LoginDto loginDto = new LoginDto(email, password);
        UserDto userDto = personService.checkLogin(loginDto);

        if (Objects.isNull(userDto)) {

            userDto = new UserDto();
            userDto.setName(name);
            userDto.setSurname(surname);
            personService.addPerson(userDto, loginDto);
            ModelAndView modelAndView = new ModelAndView("index");
            modelAndView.addObject("result", "Good");
            modelAndView.addObject("name", userDto.getName());
            modelAndView.addObject("surname", userDto.getSurname());
            return modelAndView;
        } else {

            ModelAndView modelAndView = new ModelAndView("index");
            modelAndView.addObject("result", "Error login");
            return modelAndView;
        }
    }

    @RequestMapping(value = "/by/{id}", method = RequestMethod.GET)
    public ModelAndView getById(@PathVariable int id) {
        ProductsDto productsDto = personService.getProductById(id);
        ModelAndView modelAndView = new ModelAndView("result");
        modelAndView.addObject("item", productsDto);
        modelAndView.addObject("id", id);

        return modelAndView;
    }

    @RequestMapping(value = "/result", method = RequestMethod.GET)
    public ModelAndView getResultPage() {
        ModelAndView modelAndView = new ModelAndView("result");
        modelAndView.addObject("result", "Working page");
        return modelAndView;
    }

    @RequestMapping(value = "/id", method = RequestMethod.GET)
    private void getPersonById(@RequestParam(value = "id", required = false) Integer id) {
        System.out.println("get person by id  = " + id);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    private void createPerson(@RequestBody UserDto personDto) {
        System.out.println("create person by name = " + personDto.toString());
    }

    @RequestMapping(value = "/", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    private void updatePerson(@RequestBody UserDto personDto) {
        System.out.println("update person by " + personDto.toString());
    }

    @RequestMapping(value = "/", method = RequestMethod.DELETE)
    private void deletePersonById(@RequestParam(value = "id", required = false) @PathVariable Integer id) {
        System.out.println("delete person by id  = " + id);
    }
}
