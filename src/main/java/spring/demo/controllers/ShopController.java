package spring.demo.controllers;

import java.util.Arrays;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import spring.demo.dto.OrderDto;
import spring.demo.dto.ProductsDto;
import spring.demo.dto.UserDto;
import spring.demo.services.PersonService;
import spring.demo.services.ShopService;
import spring.demo.util.SessionUtil;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


@Controller
@RequestMapping("/shop")
public class ShopController {
    @Autowired
    private ShopService shopService;

    @Autowired
    private SessionUtil sessionUtil;

    @Autowired
    private PersonService personService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView getResultPageProducts() {
        System.out.println("ID: "+PersonController.id);
        List<ProductsDto> list = shopService.getProducts();
        List<ProductsDto> list1 = shopService.getProducts1();
        List<ProductsDto> list2 = shopService.getProducts2();
        ModelAndView modelAndView = new ModelAndView("result");
        modelAndView.addObject("list", list);
        modelAndView.addObject("list1", list1);
        modelAndView.addObject("list2", list2);

        return modelAndView;
    }

    @RequestMapping(value = "/at/{id}", method = RequestMethod.GET)
    public ModelAndView deleteId(@PathVariable int id, HttpServletRequest request) throws Exception {
        Integer userId = sessionUtil.getSession(request);
        shopService.deleteId(id, userId);
        ModelAndView modelAndView = new ModelAndView("done");
        return modelAndView;
    }

    @RequestMapping(value = "/buy", method = RequestMethod.GET)
    public ModelAndView getProductFromBasketoBuy(HttpServletRequest request, HttpSession httpSession) throws Exception {
        Integer userId = sessionUtil.getSessionn(httpSession);
        UserDto user = sessionUtil.getPerson(request);

        List<ProductsDto> list = shopService.getProductsBasket(userId);
        OrderDto orderDto = shopService.getCard(userId);


        ModelAndView modelAndView = new ModelAndView("buy");


        modelAndView.addObject("list", list);
        modelAndView.addObject("userName", user.getName());
        modelAndView.addObject("userSurname", user.getSurname());
        modelAndView.addObject("userEmail", user.getEmail());
        modelAndView.addObject("orderNameCard", orderDto.getName_card());
        modelAndView.addObject("orderNumberCard", orderDto.getNumber_card());
        modelAndView.addObject("orderCvc", orderDto.getCvc());
        modelAndView.addObject("orderExpDate", orderDto.getExp_date());


        int allPrice = 0;
        for (ProductsDto prod : list) {
            allPrice += prod.getPrice();
        }

        modelAndView.addObject("allPrice", allPrice);
        return modelAndView;
    }

    @RequestMapping(value = "/order/{allPrice}", method = RequestMethod.GET)
    public ModelAndView getOrders(@PathVariable int allPrice,
                                  @RequestParam(value = "address", required = false) String address,
                                  @RequestParam(value = "city", required = false) String city,
                                  HttpServletRequest request) throws Exception {
        Integer userId = sessionUtil.getSession(request);

        UserDto userDto = new UserDto();
        userDto.setAddress(address);
        userDto.setCity(city);
        personService.updateAddress(userId, userDto);
        UserDto user = sessionUtil.getPerson(request);

        List<ProductsDto> list = shopService.getProductsBasket(userId);
        OrderDto orderDto = shopService.getCard(userId);


        ModelAndView modelAndView = new ModelAndView("pay");


        modelAndView.addObject("list", list);
        modelAndView.addObject("userName", user.getName());
        modelAndView.addObject("userSurname", user.getSurname());
        modelAndView.addObject("userEmail", user.getEmail());
        modelAndView.addObject("userAddress", user.getAddress());
        modelAndView.addObject("userCity", user.getCity());
        modelAndView.addObject("orderNameCard", orderDto.getName_card());
        modelAndView.addObject("orderNumberCard", orderDto.getNumber_card());
        modelAndView.addObject("orderCvc", orderDto.getCvc());
        modelAndView.addObject("orderExpDate", orderDto.getExp_date());

        shopService.deleteAll(userId);
        shopService.updateCounters(userId,allPrice);

        return modelAndView;
    }


    @RequestMapping(value = "/by/{id}", method = RequestMethod.GET)
    public ModelAndView getById(@PathVariable int id) throws Exception {

        List<ProductsDto> list = shopService.getProductById(id);
        ModelAndView modelAndView = new ModelAndView("good");
        modelAndView.addObject("list", list);

        return modelAndView;
    }

    @RequestMapping(value = "/opa/{id}", method = RequestMethod.POST)
    public ModelAndView AddIdBasket(@PathVariable int id, HttpServletRequest request) throws Exception {
        Integer userId = sessionUtil.getSession(request);
        shopService.addId(id, userId);
        ModelAndView modelAndView = new ModelAndView("done");
        return modelAndView;
    }

    @RequestMapping(value = "/bas/", method = RequestMethod.GET)
    public ModelAndView getProductFromBasket(HttpServletRequest request, HttpSession httpSession) throws Exception {
        Cookie [] cookies = request.getCookies();

        for (Cookie cookie:cookies) {
            System.out.println(cookie.getName());
            System.out.println(cookie.getValue());
        }

        Integer userId = sessionUtil.getSessionn(httpSession);

        List<ProductsDto> list = shopService.getProductsBasket(userId);
        ModelAndView modelAndView = new ModelAndView("basket");
        modelAndView.addObject("list", list);

        return modelAndView;
    }

    @RequestMapping(value = "/exit", method = RequestMethod.GET)
    public ModelAndView Exit(HttpServletRequest request) throws Exception {

        ModelAndView modelAndView = new ModelAndView("index");
        return modelAndView;
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView Home(HttpServletRequest request) throws Exception {
        ModelAndView modelAndView = new ModelAndView("home");
        return modelAndView;
    }
}
