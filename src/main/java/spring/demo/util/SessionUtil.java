package spring.demo.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import spring.demo.dto.OrderDto;
import spring.demo.dto.UserDto;
import spring.demo.repository.PersonRepository;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Objects;

@Component
public class SessionUtil {

    @Autowired
    private PersonRepository personRepository;

    public Integer getSession(HttpServletRequest request) throws Exception {

        System.out.println("REQUEST: "+request.getSession());
        HttpSession session = request.getSession();
        Object value = session.getAttribute("session");
        System.out.println("VALUE: "+value);

        if (Objects.isNull(value)) {
            throw new Exception("Session value not found");
        }
        return personRepository.getIdBySession(String.valueOf(value));
    }

    public Integer getSessionn(HttpSession request) throws Exception {

        Object value = request.getAttribute("session");
        System.out.println("VALUEEEE: "+value);

        if (Objects.isNull(value)) {
            throw new Exception("Session value not found");
        }
        return personRepository.getIdBySession(String.valueOf(value));
    }
    public UserDto getPerson(HttpServletRequest request) throws Exception {
        HttpSession session = request.getSession();
        Object value = session.getAttribute("session");

        if (Objects.isNull(value)) {
            throw new Exception("Session value not found");
        }
        return personRepository.getPerson(String.valueOf(value));
    }
}
