<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href="/result.css" rel="stylesheet" type="text/css">

    <title> Products</title>
</head>
<body>
<header>
    <div id="logo" onclick="slowScroll('#top')">
        <span>Магазин</span>
    </div>
    <div id="about">
        <a href="#" onclick="slowScroll('#overview')" title="ЛЕГКОВЫЕ">ЛЕГКОВЫЕ</a>
        <a href="#" onclick="slowScroll('#main')" title="ДЖИПЫ">ПИКАПЫ</a>
        <a href="#" onclick="slowScroll('#contacts')" title="ФУРГОНЫ">ФУРГОНЫ</a>
    </div>
</header>
<div id="panel"> >
    <div id="hidden_panel">
        <a href="/shop/"> <button type="submit" class="butt">КАБИНЕТ</button></a>
        <br>
        <a href="/shop/login"> <button type="submit" class="butt">ГЛАВНАЯ</button></a>
        <br>
        <a href="/shop/bas/"> <button type="submit" class="butt">КОРЗИНА</button></a>
        <br>
        <a href="/shop/exit"> <button type="submit" class="butt">ВЫХОД</button></a>

    </div>
</div>

<div id="overview">
    <h2>ЛЕГКОВЫЕ</h2>
    <h4>категория B</h4>
    <c:forEach var="item" items="${list}">
        <div class="img">
            <form id="form-1" class="tab-form" action="/shop/by/${item.id}" method="get">
                <button type="submit">
                <span>
                   <tr><img src="${item.icon_base64}"/><td>${item.brand}</td> <td>${item.name}</td></tr>
                <br>
                </span>
                </button>
            </form>
        </div>
    </c:forEach>
</div>

<div id="main">
    <h2>ПИКАПЫ</h2>
    <h4>категория C</h4>

    <c:forEach var="item" items="${list1}">
    <div class="img">
        <form id="form-2" class="tab-form" action="/shop/by/${item.id}" method="get">
            <button type="submit">
                <span>
                    <tr><img src="${item.icon_base64}"/><td>${item.brand}</td> <td>${item.name}</td></tr>
                <br>
                </span>
            </button>
        </form>
    </div>
    </c:forEach>
<div id="contacts">
    <h2>ФУРГОНЫ</h2>
    <h4>категория D</h4>

    <c:forEach var="item" items="${list2}">
        <div class="img">
            <form id="form-3" class="tab-form" action="/shop/by/${item.id}" method="get">
                <button type="submit">
                <span>
              <tr><img src="${item.icon_base64}"/><td>${item.brand}</td> <td>${item.name}</td></tr>
                <br>
                </span>
                </button>
            </form>
        </div>
    </c:forEach>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>

    function slowScroll(id) {
        $('html, body').animate({
            scrollTop: $(id).offset().top
        }, 500);
    }

    $(document).on("scroll", function () {
        if ($(window).scrollTop() === 0)
            $("header").removeClass("fixed");
        else
            $("header").attr("class", "fixed");
    });
</script>
</body>
</html>
