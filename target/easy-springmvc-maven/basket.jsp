<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href="/basket.css" rel="stylesheet" type="text/css">

    <title> design form</title>

</head>
<body>

<div id="overview">
    <h2>КОРЗИНА</h2>
<c:forEach var="item" items="${list}" begin="0" end="0">
    <div style="display:flex;justify-content:center;align-items:center;">
        <a href="/shop/buy/${item.id}" class="butt">ОФОРМИТЬ ЗАКАЗ</a>
    </div>
</c:forEach>
    <c:forEach var="item" items="${list}">
        <div class="img">
            <tr><img src="${item.icon_base64}"/>
                <td>${item.brand}</td>
                <td>${item.name}</td>
            </tr>
            <div style="display:flex;justify-content:center;align-items:center;">
                <a href="/shop/at/${item.id}" class="butt">УДАЛИТЬ</a>
            </div>

        </div>
    </c:forEach>
</div>
<div id="panel"> >i
    <div id="hidden_panel">
        <a href="/shop/"> <button type="submit" class="butt1">КАБИНЕТ</button></a>
        <br>
        <a href="/shop/login"> <button type="submit" class="butt1">ГЛАВНАЯ</button></a>
        <br>
        <a href="/shop/"> <button type="submit" class="butt1">КАТАЛОГ</button></a>
        <br>
        <a href="/shop/exit"> <button type="submit" class="butt1">ВЫХОД</button></a>
    </div>
</div>
</body>
</html>
