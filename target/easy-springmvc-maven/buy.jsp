<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href="/buy.css" rel="stylesheet" type="text/css">
    <title> Products</title>
</head>
<body>
<div class="row">
    <div class="col-75">
        <div class="container">
            <form id="form-1" class="tab-form" action="/shop/order/${allPrice}" method="get">
                <div class="row">
                    <div class="col-50">
                        <h3>Платежный адрес</h3>
                        <div class="row">
                            <div class="col-50">
                                <label for="name"><i class="fa fa-user"></i> Имя</label>
                                <input type="text" id="name" name="name" value="${userName}" readonly>
                            </div>
                            <div class="col-50">
                                <label for="surname"><i class="fa fa-user"></i> Фамилия</label>
                                <input type="text" id="surname" name="surname"  value="${userSurname}" readonly>
                            </div>
                        </div>
                        <label for="email"><i class="fa fa-envelope"></i> Email</label>
                        <input type="text" id="email" name="email" value="${userEmail}" readonly >
                        <label for="address"><i class="fa fa-address-card-o"></i> Адрес</label>
                        <input type="text" id="address" name="address" placeholder="улица Абая 46">
                        <label for="city"><i class="fa fa-institution"></i> Город</label>
                        <input type="text" id="city" name="city" placeholder="Алматы">
                    </div>

                    <div class="col-50">
                        <h3>Payment</h3>
                        <label for="nameCard">Name on Card</label>
                        <input type="text" id="nameCard" name="nameCard" value="${orderNameCard}" readonly>
                        <label for="numberCard">Credit card number</label>
                        <input type="text" id="numberCard" name="" value="${orderNumberCard}" readonly>
                        <label for="expdate">Exp Month</label>
                        <input type="text" id="expdate" name="expdate" value="${orderExpDate}" readonly>
                        <label for="cvc">CVC</label>
                        <input type="text" id="cvc" name="cvv" value="${orderCvc}" readonly>
                    </div>

                </div>
                <input type="submit" value="Продолжить оформление заказа" class="btn">
            </form>
        </div>
    </div>
    <div class="col-25">
        <div class="container">
            <h4>Cart <span class="price" style="color:black"><i class="fa fa-shopping-cart"></i> <b>${list.size()}</b></span></h4>
            <c:forEach var="item" items="${list}">
                <p><a href="#">
                    <td>${item.brand}</td> <td>${item.name}</td>
                </a><span class="price"><td>$${item.price}</td></span></p>
                <hr>
            </c:forEach>
            <p>Всего <span class="price" style="color:black"><b>${allPrice}$</b></span></p>
        </div>
    </div>
</div>
<div id="panel"> >
    <div id="hidden_panel">
        <a href="/shop/"> <button type="submit" class="butt1">КАБИНЕТ</button></a>
        <br>
        <a href="/shop/login"> <button type="submit" class="butt1">ГЛАВНАЯ</button></a>
        <br>
        <a href="/shop/"> <button type="submit" class="butt1">КАТАЛОГ</button></a>
        <br>
        <a href="/shop/bas"> <button type="submit" class="butt1">КОРЗИНА</button></a>
        <br>
        <a href="/shop/exit"> <button type="submit" class="butt1">ВЫХОД</button></a>
    </div>
</div>
</body>


</html>
