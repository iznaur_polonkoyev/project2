create table public.category_products
(
    id            serial primary key,
    name_category varchar(255),
    icon_base64   text
);

create table public.products
(
    id            serial primary key,
    category_id   integer,
    icon_base64   text,
    name_product  varchar(1000),
    brand_product varchar(1000),
    description   varchar(1000),
    price         integer,
    count         integer,
    created_date  timestamp,
    foreign key (category_id) references public.category_products (id)
);

create table public.users
(
    id       serial primary key,
    email    varchar(255),
    password varchar(1000),
    name     varchar(255),
    surname  varchar(255)
);


create table public.basket
(
    id         serial primary key,
    product_id integer,
    user_id    integer,
    foreign key (product_id) references public.products (id),
    foreign key (user_id) references public.users (id)
);

create table public.card
(
    id          serial primary key,
    user_id     integer,
    number_card varchar(12),
    cvc         varchar(10),
    cash        integer,
    foreign key (user_id) references public.users (id)
);

create table public.user_contacts
(
    id        serial primary key,
    user_id   integer,
    telephone varchar(255),
    facebook  varchar(255),
    foreign key (user_id) references public.users (id)
)
