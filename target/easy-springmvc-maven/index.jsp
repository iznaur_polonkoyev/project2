<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href="/index.css" rel="stylesheet" type="text/css">

    <title> design form</title>

</head>

<body>
<div id="intro">
    <div class="video">
        <video class="video__media" src="/videos/3.mp4" autoplay muted loop></video>
    </div>

    <div class="intro__content">
        <div class="container1">
            <div class="dws-wrapper">
                <div class="dws-form">

                    <input type="radio" id="tab-1" name="tabs" checked>
                    <label class="tab" for="tab-1" title="Вкладка 1">Авторизация</label>

                    <input type="radio" name="tabs" id="tab-2">
                    <label class="tab" for="tab-2" title="Вкладка 2">Регистрация</label>

                    <form id="form-1" class="tab-form" action="/person/login" method="post">
                        <div class="box-input">
                            <input class="input" id="username" name="username" type="text" required />
                            <label>Введите E-mail</label>
                        </div>

                        <div class="box-input">
                            <input class="input" id="password" name="password"  type="password" required/>
                            <label>Введите пароль</label>
                        </div>

                        <input  class="button" type="submit" value="ВОЙТИ">

                    </form>

                    <form id="form-2" class="tab-form" action="/person/registration" method="post">
                        <div class="box-input">
                            <input class="input" id="email" name="email"  type="email" required/>
                            <label>Введите E-mail</label>
                        </div>
                        <div class="box-input">
                            <input class="input" id="name" name="name"  type="text" required/>
                            <label>Введите Имя</label>
                        </div>
                        <div class="box-input">
                            <input class="input" id="surname" name="surname"  type="text" required/>
                            <label>Введите Фамилию</label>
                        </div>
                        <div class="box-input">
                            <input class="input" id="password1" name="password"  type="password" required/>
                            <label>Введите пароль</label>
                        </div>

                        <input  class="button" type="submit" value="РЕГИСТРАЦИЯ">

                    </form>

                </div>
            </div>
        </div>
    </div>
</div>

</body>


</html>
